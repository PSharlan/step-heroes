package by.itstep.heroes.stepheroes.integration;

import by.itstep.heroes.StepHeroesApplication;
import by.itstep.heroes.stepheroes.integration.initializer.MySqlInitializer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
@ContextConfiguration(initializers = MySqlInitializer.class,
        classes = StepHeroesApplication.class)
public class AbstractIntegrationTest {

}
