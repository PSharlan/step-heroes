package by.itstep.heroes.entity;

import by.itstep.heroes.entity.enums.Characteristic;
import by.itstep.heroes.entity.enums.ArtifactType;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "artifacts")
public class ArtifactEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "image_url", nullable = false)
    private String imageUrl;

    @Column(name = "type_artifact", nullable = false)
    @Enumerated(EnumType.STRING)
    private ArtifactType typeArtifact;

    @Column(name = "added_characteristic")
    @Enumerated(EnumType.STRING)
    private Characteristic addCharacteristic;

    @Column(name = "min_hero_level")
    private Integer minHeroLevel;

    @Column(name = "number_of_added_characteristic")
    private Integer numberOfAddedCharacteristic;

    @ManyToMany
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinTable(
            name = "artifact_inventory",
            joinColumns = {@JoinColumn(name = "artifact_id")},
            inverseJoinColumns = {@JoinColumn(name = "inventory_id ")}
    )
    private List<InventoryEntity> inventories = new ArrayList<>();
}
