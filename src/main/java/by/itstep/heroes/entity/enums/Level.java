package by.itstep.heroes.entity.enums;

public enum Level {

    FIRST(1, 500),
    SECOND(2, 600),
    THIRD(3, 700),
    FOURTH(4, 800),
    FIFTH(5, 900),
    SIXTH(6, 1000),
    SEVENTH(7, 1100),
    EIGHTH(8, 1200),
    NINTH(9, 1300),
    TENT(10, 1400);

    private Integer level;
    private Integer experienceToTheNextLevel;

    Level(Integer level, Integer experienceToTheNextLevel) {
        this.level = level;
        this.experienceToTheNextLevel = experienceToTheNextLevel;
    }
}
