package by.itstep.heroes.entity.enums;


public enum ArtifactType {

    HEAD,
    RIGHT_HAND,
    LEFT_HAND,
    BOTH_HANDS,
    BODY,
    LEGS
}
