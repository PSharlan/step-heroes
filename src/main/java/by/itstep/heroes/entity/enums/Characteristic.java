package by.itstep.heroes.entity.enums;

public enum Characteristic {

    DAMAGE,
    HP,
    DEFENCE
}
