package by.itstep.heroes.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "inventories")
public class InventoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "inventory")
    private HeroEntity hero;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "active_artifact", joinColumns = @JoinColumn(name = "inventory_id"))
    @Column(name = "artifact_id")
    private Set<Integer> activeArtifacts = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "inventories")
    private List<ArtifactEntity> artifacts = new ArrayList<>();
}
