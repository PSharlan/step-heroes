package by.itstep.heroes.entity;

import by.itstep.heroes.entity.enums.Level;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "heroes")
public class HeroEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "student_id", nullable = false, unique = true)
    private Long studentId;

    @Column(name = "level", nullable = false)
    @Enumerated(EnumType.STRING)
    private Level level;

    @Column(name = "score", nullable = false)
    private Long score;

    @Column(name = "current_experience")
    private Integer currentExperience;

    @Column(name = "free_points")
    private Integer freePoints;

    @Column(name = "base_hp", nullable = false)
    private Long baseHp;

    @Column(name = "base_damage", nullable = false)
    private Long baseDamage;

    @Column(name = "base_defense", nullable = false)
    private Long baseDefense;

    @Column(name = "intelligence", nullable = false)
    private Long intelligence;

    @JoinColumn(name = "agility", nullable = false)
    private Long agility;

    @Column(name = "strength", nullable = false)
    private Long strength;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "inventory_id", referencedColumnName = "id", nullable = false)
    private InventoryEntity inventory;

}
