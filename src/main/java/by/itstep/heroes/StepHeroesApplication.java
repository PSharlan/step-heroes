package by.itstep.heroes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepHeroesApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepHeroesApplication.class, args);
	}

}
